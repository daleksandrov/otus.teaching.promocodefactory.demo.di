﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.ConsoleHost.Dto;

namespace Otus.Teaching.PromoCodeFactory.ConsoleHost.Views
{
    public class EmployeeListView
    {
        private readonly List<EmployeeShortInfoDto> _info;

        public EmployeeListView(List<EmployeeShortInfoDto> info)
        {
            _info = info;
        }

        public void Show()
        {
            Console.WriteLine("Список сотрудников");
            foreach (var employee in _info)
            {
                Console.WriteLine($"{nameof(employee.Id)}: {employee.Id}, " +
                                  $"{nameof(employee.FullName)}: {employee.FullName}, " +
                                  $"{nameof(employee.Email)}: {employee.Email}");
            }
        }
    }
}