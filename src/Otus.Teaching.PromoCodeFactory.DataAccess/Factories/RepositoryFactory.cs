﻿using System;
using Microsoft.Extensions.Options;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Factories
{
    public class RepositoryFactory
        : IRepositoryFactory
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly bool _isTest;
        
        public RepositoryFactory(IOptions<DataAccessSettings> options, IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _isTest = options.Value.IsTestData;
        }
        
        public IRepository<T> GetRepository<T>() where T : BaseEntity
        {
            if(_isTest)
                return (IRepository<T>)_serviceProvider.GetService(typeof(NotImplementedRepository<T>));
        
            return (IRepository<T>)_serviceProvider.GetService(typeof(InMemoryRepository<T>));
        }
    }
}