﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataAccessSettings
    {
        public bool IsTestData { get; set; }
    }
}